# iceCode代码高亮

#### 官方
+ iceCode 官方网站 [https://www.iceui.net/iceCode.html](https://www.iceui.net/iceCode.html)
+ iceCode 示例文档 [https://www.iceui.net/iceCode/doc.html](https://www.iceui.net/iceCode/doc.html)

#### 介绍
iceCode是一款简约风格的代码高亮插件，纯原生JavaScript开发，体型娇小，无任何依赖，整个编辑器只有一个文件，功能却很不平凡！简约的唯美设计，简洁、极速、使用它的时候不需要引用jQuery、font、css……等文件，因为整个代码高亮只是一个Js！
iceCode官方群：324415936

#### 优点
+ 纯原生开发，无任何依赖，冰清玉洁
+ 响应式布局，适应任何分辨率的设备
+ 整个代码高亮插件只有一个文件，高效便捷
+ 简约的唯美设计，简洁、极速

#### 使用
```html
<div id="editor"> 欢迎使用iceCode富文本编辑器 </div>
```
```javascript
//第一步：创建实例化对象
var e = new ice.editor('content');

//第二步：配置图片或附件上传提交表单的路径
e.uploadUrl="/iceCode/src/upload.php";

//第三步：配置菜单（默认加载全部，无需配置）
e.menu = [
  'backColor',                 //字体背景颜色
  'fontSize',                  //字体大小
  'foreColor',                 //字体颜色
  'bold',                      //粗体
  'italic',                    //斜体
  'underline',                 //下划线
  'strikeThrough',             //删除线
  'justifyLeft',               //左对齐
  'justifyCenter',             //居中对齐
  'justifyRight',              //右对齐
  'indent',                    //增加缩进
  'outdent',                   //减少缩进
  'insertOrderedList',         //有序列表
  'insertUnorderedList',       //无序列表
  'superscript',               //上标
  'subscript',                 //下标
  'createLink',                //创建连接
  'unlink',                    //取消连接
  'insertHorizontalRule',      //水平线
  'table',                     //表格
  'files',                     //附件
  'music',                     //音乐
  'video',                     //视频
  'insertImage',               //图片
  'removeFormat',              //格式化样式
  'code',                      //源码
  'line'                       //菜单分割线
];

//第四步：创建
e.create();
```

#### 设置编辑器尺寸
```javascript
var e = new ice.editor('content');
e.width='700px';   //宽度
e.height='300px';  //高度
e.create();
```

#### 禁用编辑器
```javascript
var e = new ice.editor('content');
e.disabled=true;
e.create();
```

#### 获取内容
```javascript
var e = new ice.editor('content');
console.log(e.getHTML());  //获取HTML格式内容
console.log(e.getText());  //获取Text格式内容
```

#### 设置内容
```javascript
var e = new ice.editor('content');
e.setValue('hello world！');
```

#### 追加内容
```javascript
var e = new ice.editor('content');
e.addValue('hello world！');
```